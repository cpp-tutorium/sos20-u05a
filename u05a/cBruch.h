#pragma once
class cBruch
{
	int zaehler;
	int nenner;
	int ggT(int a, int b);
	void kuerzen();

public:
	cBruch(int zaehler_in = 0, int nenner_in = 1);
	void ausgabe();
	friend cBruch add(cBruch a, cBruch b);
	friend cBruch sub(cBruch a, cBruch b);
	friend cBruch div(cBruch a, cBruch b);
	friend cBruch mul(cBruch a, cBruch b);
	friend int vergleich(cBruch a, cBruch b);
};

