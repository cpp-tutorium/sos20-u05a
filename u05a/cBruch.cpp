#include "cBruch.h"
#include <iostream>

int cBruch::ggT(int a, int b)
{
	if (b == 0)
	{
		return a;
	}
	return cBruch::ggT(b, a % b);
}

void cBruch::kuerzen()
{
	int teiler = 0;
	if (nenner < zaehler) {
		teiler = ggT(zaehler, nenner);
	}
	else if (nenner > zaehler) {
		teiler = ggT(nenner, zaehler);
	}
	else {
		teiler = nenner;
	}
	nenner /= teiler;
	zaehler /= teiler;
}

cBruch::cBruch(int zaehler_in, int nenner_in)
{
	if (nenner_in < 0) {
		nenner_in *= -1;
		zaehler_in *= -1;
	}
	else if (nenner_in == 0) {
		nenner_in = 1;
	}
	nenner = nenner_in;
	zaehler = zaehler_in;

	kuerzen();
}

void cBruch::ausgabe()
{
	std::cout << zaehler << "/" << nenner << std::endl;
	std::cout << zaehler / (double)nenner << std::endl;
}

cBruch add(cBruch a, cBruch b)
{
	int zaehler = a.zaehler * b.nenner + a.nenner * b.zaehler;
	int nenner = b.nenner * a.nenner;
	cBruch c = cBruch(zaehler, nenner);
	c.kuerzen();
	return c;
}

cBruch sub(cBruch a, cBruch b)
{
	int zaehler = a.zaehler * b.nenner - b.zaehler * a.nenner;
	int nenner = b.nenner * a.nenner;
	cBruch c = cBruch(zaehler, nenner);
	c.kuerzen();
	return c;
}

cBruch div(cBruch a, cBruch b)
{
	int zaehler = a.zaehler * b.nenner;
	int nenner = a.nenner * b.zaehler;
	cBruch c = cBruch(zaehler, nenner);
	c.kuerzen();
	return c;
}

cBruch mul(cBruch a, cBruch b)
{
	int zaeler = a.zaehler * b.zaehler;
	int nenner = a.nenner * b.nenner;
	cBruch c = cBruch(zaeler, nenner);
	c.kuerzen();
	return c;
}

int vergleich(cBruch a, cBruch b)
{
	double d1 = a.zaehler / (double)a.nenner;
	double d2 = b.zaehler / (double)b.nenner;

	if (d1 > d2) {
		return 1;
	}else if (d2 > d1) {
		return -1;
	}
	return 0;
}
