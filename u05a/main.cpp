#include "cBruch.h"
#include <iostream>

using namespace std;
int main() {
	/*cBruch b1 = cBruch(2, 3);
	cBruch b2 = cBruch(2, 4);

	b1.ausgabe();
	b2.ausgabe();

	cBruch b3 = add(b1, b2);
	b3.ausgabe();

	cBruch b4 = sub(b1, b2);
	cBruch b5 = mul(b1, b2);
	cBruch b6 = div(b1, b2);

	b4.ausgabe();
	b5.ausgabe();
	b6.ausgabe();

	int a = vergleich(b2, b2);
	std::cout << a << std::endl;*/


	cBruch cBArr[8];

	cBArr[0] = cBruch(3, 4);
	cBArr[1] = cBruch(24, -6);
	cBArr[2] = cBruch(-5, -3);
	cBArr[3] = cBruch(-14, 22);
	cBArr[4] = cBruch(21, 45);
	cBArr[5] = cBruch(7, -9);
	cBArr[6] = cBruch(2, 3);

	cBArr[7] = add(cBArr[0], cBArr[1]);
	cBArr[7].ausgabe();

	cBArr[7] = sub(cBArr[2], cBArr[3]);
	cBArr[7].ausgabe();

	cBArr[7] = mul(cBArr[4], cBArr[5]);
	cBArr[7].ausgabe();

	div(cBArr[6], cBArr[7]).ausgabe();

	cout << "Vergleich 1: " << vergleich(cBArr[0], cBArr[2]) << endl;
	cout << "Vergleich 2: " << vergleich(cBArr[1], cBArr[3]) << endl;
	cout << "Vergleich 3: " << vergleich(cBArr[4], cBArr[6]) << endl;
	cout << "Vergleich 4: " << vergleich(cBArr[5], cBArr[7]) << endl;

	return 0;
}